/**
 * Created by ruilopes on 2/26/2019.
 */

trigger OnAccountManipulation on Account (before insert, before update) {
    List<Contact> conWithPC = [SELECT Id FROM Contact WHERE Primary_Contact__c = true];
    for (Account acc : trigger.new) {
        for (Integer i = 0; i < conWithPC.size(); i++) {
            if (acc.Primary_Contact__c == conWithPC[i].Id) {
                acc.addError('Mag niet!');
            }
        }

    }
    //update conWithPC;
}
trigger saveButton on Employee__c (before insert, before update) {
    if(Trigger.isInsert) {
        for (Employee__c emp : Trigger.New) {
            EmployeeController.calculateSalaryPerMonth(emp.Salary_Per_Day__c, emp.No_of_Days__c, emp.Bonus__c, emp.Id);
        }
    }
    
    if(Trigger.isUpdate) {
        List<Employee__c> employee_new = Trigger.New;
        List<Employee__c> employee_old = Trigger.Old;
        
        
        for (Integer i = 0; i < employee_new.size(); i++) {
            if(employee_new[i].Id != employee_old[i].Id) {
                EmployeeController.calculateSalaryPerMonth(employee_new[i].Salary_Per_Day__c, employee_new[i].No_of_Days__c, employee_new[i].Bonus__c, employee_new[i].Id);
            }
        }
    }
}
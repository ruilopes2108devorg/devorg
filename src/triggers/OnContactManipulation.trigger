/**
 * Created by ruilopes on 2/26/2019.
 */

trigger OnContactManipulation on Contact (after insert, after update, before delete, before update) {


    if (trigger.isBefore) {

        if (Trigger.isUpdate) {

            //Creates lists for trigger new and old
            List<Contact> oldList = trigger.old;
            List<Contact> newList = trigger.new;

            //Makes sure you cannot uncheck a primary contact checkbox
            for (Integer i = 0; i < newList.size(); i++) {

                //Check if old value is true and new value is false
                if (oldList[i].Primary_Contact__c == true && newList[i].Primary_Contact__c == false) {
                    newList[i].addError('You cannot uncheck the primary contact! ');
                } else if (oldList[i].AccountId != newList[i].AccountId) {
                    newList[i].addError('You cannot change the primary contact\'s Account!');
                }
            }
        }

        if (trigger.isDelete) {

            // Loop every contact in the trigger.old
            for (Contact con : trigger.old) {

                //Report error message if primary contact is deleted
                if (con.Primary_Contact__c == true) {
                    con.addError('Je mag geen primary contact verwijderen');
                }
            }
        }
    }

    if (trigger.isAfter) {

        //Create Account and Contact Lists
        List<Account> accList = new List<Account>();

        List<Contact> conList = new List<Contact>();

        //Loop all the new/updated contacts
        for (Contact con : trigger.new) {

            //Check if contacts have an Account and have the primary contact checkbox checked.
            if (con.AccountId != null && con.Primary_Contact__c == true) {

                //Check the Account of the contact
                Account acc = [SELECT Id, Primary_Contact__c FROM Account WHERE Id = :con.AccountId];

                //Make the contact, primary contact of the account
                acc.Primary_Contact__c = con.Id;

                //Adds the contact to the list
                accList.add(acc);

                //Create a list of contacts who have primary contact checkbox checked
                conList = [SELECT Id FROM Contact WHERE AccountId = :con.AccountId AND Primary_Contact__c = true AND Id != :con.Id];

                //Uncheck the other primary contacts of the contacts
                for (Contact contact : conList) {
                    contact.Primary_Contact__c = false;
                }

            } else if (con.AccountId == null && con.Primary_Contact__c == true) {

                //If contact has no Account linked, an error will be sent.
                con.addError('Please input account.');

            } else if (con.AccountId != null && con.Primary_Contact__c == false) {

                //Creates a list of contacts with the same Account id as the contact in the loop
                List<Contact> conList2 = [SELECT Id FROM Contact WHERE AccountId = :con.AccountId];


                //Check if the list has exactly 1 contact
                if (conList2.size() == 1) {

                    //Makes the contact, primary contact of his account
                    conList2[0].Primary_Contact__c = true;

                    update conList2;
                }
            }


            update conList;

            update accList;

        }


    }
}
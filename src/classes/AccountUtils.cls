/**
 * Created by ruilopes on 4/23/2019.
 */

public with sharing class AccountUtils {
    public static List<Account> accountsByState(String abbreviation) {
        List<Account> accList = [SELECT Id, Name, BillingState FROM Account WHERE BillingState = :abbreviation];
        return accList;
    }
}
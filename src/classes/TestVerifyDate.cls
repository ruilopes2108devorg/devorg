/**
 * Created by ruilopes on 2/25/2019.
 */

@IsTest
private class TestVerifyDate {
    @IsTest
    static void testBehavior() {
        Date pastDate = Date.newInstance(2017,08,08);

        Date date1 = VerifyDate.checkDates(pastDate, System.today());
        Date date2 = VerifyDate.checkDates(System.today() - 1, System.today());
        Date date3 = VerifyDate.checkDates(Date.newInstance(3017,08,30), System.today());

        System.assertEquals(date1, Date.newInstance(2017,08,31));
        System.assertEquals(date2, System.today());
        System.assertEquals(date3, Date.newInstance(3017,08,31));
    }
}
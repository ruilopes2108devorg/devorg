/**
 * Created by ruilopes on 2/25/2019.
 */

public with sharing class TestFactory {

    public static List<Account> createAccount(Integer accCount) {
        List<Account> accList = new List<Account>();

        for (Integer i = 0; i < accCount; i++) {
            Account acc = new Account();
            acc.Name = 'TestAccount' + NewGuid();
            accList.add(acc);
        }
        insert accList;
        return accList;
    }

    public static List<Contact> createContact(Integer conCount) {
        List<Account> acc = createAccount(1);
        List<Contact> conList = new List<Contact>();

        for (Integer i = 0; i < conCount; i++) {
            Contact con =  new Contact();

            con.FirstName = 'TestFirstName ' + i;
            con.LastName = 'TestLastName' + NewGuid();
            con.AccountId = acc[0].Id;
            conList.add(con);
        }
        insert conList;
        return conList;
    }

    public static List<Opportunity> createOpportunity(Integer oppCount) {
        List<Account> acc = createAccount(1);
        List<Opportunity> oppList = new List<Opportunity>();

        for (Integer i = 0; i < oppCount; i++) {
            Opportunity opp = new Opportunity();
            opp.Name = 'TestOpportunity' + NewGuid();
            opp.StageName = 'Prospecting';
            opp.CloseDate = System.today();
            opp.AccountId = acc[0].Id;
            oppList.add(opp);
        }
        insert oppList;
        return oppList;
    }

    private static String NewGuid() {
        Blob aes = Crypto.generateAesKey(128);
        return EncodingUtil.convertToHex(aes);
    }
}
public class TotalSalaryPerMonth {
    public static Decimal calculateSalaryPerMonth(String name) {
        List<Employee__c> emplo = new List<Employee__c>();
        emplo = [SELECT Salary_Per_Day__c, 	No_of_Days__c FROM Employee__c Where Name = :name];
        Decimal salaryPerMonth = (Decimal)emplo.get(0).Salary_Per_Day__c * (Decimal)emplo.get(0).No_of_Days__c;
        return salaryPerMonth;
    }
}
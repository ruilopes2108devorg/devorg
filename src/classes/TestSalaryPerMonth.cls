@isTest private class TestSalaryPerMonth {
    	@isTest static void testSalaryMonth() {
			Employee__c employ1 = new Employee__c(Employee_Name__c = 'Rui', Salary_Per_Day__c = 12.50,  No_of_Days__c = 20.00);
            insert employ1;
            Decimal testSalary = EmployeeController.calculateSalaryPerMonth();
            System.assertEquals(250.00, testSalary);
        }
    
    	@isTest static void testTotalSalaryMonth() {
			Employee__c employ2 = new Employee__c(Employee_Name__c = 'Jose', Salary_Per_Day__c = 100.00,  No_of_Days__c = 30.00, Bonus__c = 10);
            insert employ2;
            Decimal testTotalSalary = EmployeeController.calculateTotalSalaryPerMonth();
            System.assertEquals(3300.00, testTotalSalary);
    	}
    	
    }
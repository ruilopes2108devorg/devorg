/**
 * Created by ruilopes on 2/20/2019.
 */

public class ContactSearch {

    public static List<Contact> searchForContacts(String lastname, String mailingpostalcode) {

        List<Contact> contacts = [SELECT id, FirstName, LastName FROM Contact WHERE LastName = :lastname AND MailingPostalCode = :mailingpostalcode];

        return contacts;
    }
}
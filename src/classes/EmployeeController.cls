public with sharing class EmployeeController {
    @AuraEnabled
public static Employee__c calculateSalaryPerMonth(Decimal salary, Decimal noOfDays, Decimal bonus, String recordId) {
        //Decimal salaryMonth = salary * noOfDays;
        //Decimal salaryTotalMonth = salaryMonth * (1 + bonus / 100);
        
        //List<Employee__c> emp = new List<Employee__c>();
        //emp = [SELECT id FROM Employee__c WHERE Id = :recordId LIMIT 1];
        
    	Employee__c employeeTest = new Employee__c();
    	employeeTest.id = recordId;
    

            employeeTest.Salary_Per_Day__c = salary;
            employeeTest.No_of_Days__c = noOfDays;
            employeeTest.Bonus__c = bonus;
            //employeeTest.Salary_Per_Month__c = salaryMonth;
            //employeeTest.Total_Salary_Per_Month__c = salaryTotalMonth;

        update employeeTest; 
        return employeeTest;
    }
}
public class ContactController {
    @AuraEnabled
    public static List<Contact> retrieveContacts() {
        return [SELECT Id, Name, Phone FROM Contact LIMIT 10];
    }



}
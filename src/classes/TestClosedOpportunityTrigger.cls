/**
 * Created by ruilopes on 2/25/2019.
 */

@IsTest
private class TestClosedOpportunityTrigger {
    @IsTest
    static void testBehavior() {
        List<Opportunity> oppList = TestFactory.createOpportunity(2);
        oppList[1].StageName = 'Closed Won';

        insert oppList;

        List<Task> taskList = [SELECT Id, WhatId FROM Task WHERE WhatId IN (:oppList[0].Id, :oppList[1].Id)]; // Tasklist krijgt records alleen als WhatId

        System.assert(taskList.size() == 1);

        System.assert(oppList[0].Id != taskList[0].WhatId);
        System.assert(oppList[1].Id == taskList[0].WhatId);
    }
}
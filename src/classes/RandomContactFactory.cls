/**
 * Created by ruilopes on 2/26/2019.
 */

public with sharing class RandomContactFactory {
    public static List<Contact> generateRandomContacts(Integer numOfCon, String lastName) {
        List<Contact> conList = new List<Contact>();

        for (Integer i = 1; i <= numOfCon; i++) {
            Contact con = new Contact(FirstName = 'Test ' + i, LastName = lastName);
            conList.add(con);
        }

        return conList;
    }
}
/**
 * Created by ruilopes on 2/26/2019.
 */

@IsTest
private class TestRestrictContactByName {
    @IsTest
    static void testBehavior() {
        List<Contact> conList = TestFactory.createContact(2);
        conList[1].LastName = 'INVALIDNAME';

        try {
            insert ConList;
        } catch(Exception e) {
            System.assert(e.getMessage().contains('The Last Name "'+ conList[1].LastName + '" is not allowed for DML'));
        }
        System.assertEquals(conList[0].LastName, 'TestLastName');
        System.assertEquals(conList[1].LastName, 'INVALIDNAME');
    }
}
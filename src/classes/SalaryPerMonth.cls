public class SalaryPerMonth {
    public static Decimal calculateSalaryPerMonth(String name) {
        List<Employee__c> empl1 = new List<Employee__c>();
        empl1 = [SELECT id FROM Employee__c Where Employee__c.Name = :name LIMIT 1];
        Decimal salaryMonth = (Decimal)empl1.get(0).Salary_Per_Day__c * (Decimal)empl1.get(0).No_of_Days__c;
        return salaryMonth;
    }
    
    public static Decimal calculateTotalSalaryPerMonth(String name) {
        List<Employee__c> empl2 = new List<Employee__c>();
        empl2 = [SELECT id FROM Employee__c Where Employee__c.Name = :name LIMIT 1];
        Decimal salaryTotalMonth = ((calculateSalaryPerMonth(name) / 100) * empl2.get(0).Bonus__c) + calculateSalaryPerMonth(name);
        return salaryTotalMonth;
    }
}